# Commerce Boncard Module

## About this module

Boncard is a gift card provider based in Switzerland.
This module integrates Boncard as a gift card module into Drupal Commerce.

## Features
This module allows you to apply Boncard gift cards during the checkout.
- Boncard transactions are fieldable entities, that can easily be extended.
- Boncard have a state and are related to a workflow.
    - New > Authorized > Complete > Partially refunded > Refunded
- Boncard Giftcards are handled as Commerce adjustments and processed like payments.
- A new tab "Giftcards" will be added to the order. Related gift card transactions are visible.
- It is possible to pay a partial amount with a gift card and pay the rest with another payment method, also with a credit card offsite payment.
- Cancel / Voidance and Refund of git card payments is available in the backend.
- A valid contract Boncard AG (https://boncard.ch) is required to use this module.

## REQUIREMENTS
Drupal Commerce module is required for this module.

## INSTALLATION
- Using composer require drupal/commerce_boncard to install the module and its
dependancies.
- Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Configure your Boncard account credentials under `/admin/commerce/config/boncard`
- Edit your checkout flow under `/admin/commerce/config/checkout-flows `
  - Add the Boncard Redemption form to the sidebar step like coupon redemption
  - **Important:** Add "Boncard processing" **before** "Payment process in the "Payment" step".
- Test the process.
