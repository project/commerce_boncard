<?php

namespace Drupal\commerce_boncard;

use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a boncard transaction entity type.
 */
interface BoncardInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the boncard transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the boncard transaction.
   */
  public function getCreatedTime();

  /**
   * Sets the boncard transaction creation timestamp.
   *
   * @param int $timestamp
   *   The boncard transaction creation timestamp.
   *
   * @return \Drupal\commerce_boncard\BoncardInterface
   *   The called boncard transaction entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the parent order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order entity, or null.
   */
  public function getOrder();

  /**
   * Gets the parent order ID.
   *
   * @return int|null
   *   The order ID, or null.
   */
  public function getOrderId();

  /**
   * Gets the payment remote ID.
   *
   * @return string
   *   The payment remote ID.
   */
  public function getRemoteId();

  /**
   * Sets the payment remote ID.
   *
   * @param string $remote_id
   *   The payment remote ID.
   *
   * @return $this
   */
  public function setRemoteId($remote_id);

  /**
   * Gets the payment remote state.
   *
   * @return string
   *   The payment remote state.
   */
  public function getRemoteState();

  /**
   * Sets the payment remote state.
   *
   * @param string $remote_state
   *   The payment remote state.
   *
   * @return $this
   */
  public function setRemoteState($remote_state);

  /**
   * Gets the payment balance.
   *
   * The balance represents the payment amount minus the refunded amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment balance, or NULL if the payment does not have an amount yet.
   */
  public function getBalance();

  /**
   * Gets the card value.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL.
   */
  public function getCardValue();

  /**
   * Sets the card value.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The payment amount.
   *
   * @return $this
   */
  public function setCardValue(Price $amount);

  /**
   * Gets the payment amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL.
   */
  public function getAmount();

  /**
   * Sets the payment amount.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The payment amount.
   *
   * @return $this
   */
  public function setAmount(Price $amount);

  /**
   * Gets the refunded payment amount.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The refunded payment amount, or NULL.
   */
  public function getRefundedAmount();

  /**
   * Sets the refunded payment amount.
   *
   * @param \Drupal\commerce_price\Price $refunded_amount
   *   The refunded payment amount.
   *
   * @return $this
   */
  public function setRefundedAmount(Price $refunded_amount);

  /**
   * Gets the payment state.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The payment state.
   */
  public function getState();

  /**
   * Sets the payment state.
   *
   * @param string $state_id
   *   The new state ID.
   *
   * @return $this
   */
  public function setState($state_id);

  /**
   * Gets the payment authorization timestamp.
   *
   * @return int
   *   The payment authorization timestamp.
   */
  public function getAuthorizedTime();

  /**
   * Sets the payment authorization timestamp.
   *
   * @param int $timestamp
   *   The payment authorization timestamp.
   *
   * @return $this
   */
  public function setAuthorizedTime($timestamp);

  /**
   * Gets whether the payment has expired.
   *
   * @return bool
   *   TRUE if the payment has expired, FALSE otherwise.
   */
  public function isExpired();

  /**
   * Gets the payment expiration timestamp.
   *
   * Marks the time after which the payment can no longer be completed.
   *
   * @return int
   *   The payment expiration timestamp.
   */
  public function getExpiresTime();

  /**
   * Sets the payment expiration timestamp.
   *
   * @param int $timestamp
   *   The payment expiration timestamp.
   *
   * @return $this
   */
  public function setExpiresTime($timestamp);

  /**
   * Gets whether the payment has been completed.
   *
   * @return bool
   *   TRUE if the payment has been completed, FALSE otherwise.
   */
  public function isCompleted();

  /**
   * Gets the payment completed timestamp.
   *
   * @return int
   *   The payment completed timestamp.
   */
  public function getCompletedTime();

  /**
   * Sets the payment completed timestamp.
   *
   * @param int $timestamp
   *   The payment completed timestamp.
   *
   * @return $this
   */
  public function setCompletedTime($timestamp);

  /**
   * Sets the card number.
   *
   * @return string
   */
  public function getCardNumber();

  /**
   * Gets the card number.
   *
   * @param $card_number
   *
   * @return $this
   */
  public function setCardNumber($card_number);

  /**
   * Gets the CVC.
   *
   * @return int
   */
  public function getCvc();

  /**
   * Sets the CVC.
   *
   * @param $cvc
   *
   * @return $this
   */
  public function setCvc($cvc);

  /**
   * Gets the merchant name.
   *
   * @return string
   */
  public function getMerchantName();

  /**
   * Sets the merchant name.
   *
   * @param $merchant_name
   *
   * @return $this
   */
  public function setMerchantName($merchant_name);

  /**
   * Gets the merchant ID.
   *
   * @return string
   */
  public function getMechantId();

  /**
   * Sets the Merchant ID.
   *
   * @param $merchant_id
   *
   * @return $this
   */
  public function setMerchantId($merchant_id);

  /**
   * Gets the Terminal ID.
   *
   * @return string
   */
  public function getTerminalId();

  /**
   * Sets the Terminal ID.
   *
   * @param $terminal_id
   *
   * @return $this
   */
  public function setTerminalId($terminal_id);

}
