<?php

namespace Drupal\commerce_boncard;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderProcessorInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;

/**
 * Applies promotions to orders during the order refresh process.
 */
class BoncardOrderProcessor implements OrderProcessorInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * GiftcardOrderProcessor constructor.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Token $token) {
    $this->entityTypeManager = $entity_type_manager;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function process(OrderInterface $order) {
    /** @var \Drupal\commerce_boncard\BoncardStorageInterface $boncard_storage */
    $boncard_storage = $this->entityTypeManager->getStorage('commerce_boncard');

    $giftcards = $boncard_storage->loadMultipleByOrder($order);

    $order_total = $order->getTotalPrice();
    foreach ($giftcards as $giftcard) {

      // If order total is zero, skip any remaining giftcard.
      if ($order_total->isZero()) {
        break;
      }

      // Ignore giftcards with the wrong currency, validation should prevent
      // this from happening.
      if ($giftcard->getCardValue()->getCurrencyCode() !== $order_total->getCurrencyCode()) {
        continue;
      }

      // Decide how much of the balance is added as an adjustment.
      if ($order_total->greaterThan($giftcard->getCardValue())) {
        $amount = $giftcard->getCardValue();
      }
      else {
        $amount = $order_total;
      }

      $label = $this->t('Gift card');

      $order->addAdjustment(new Adjustment([
        'type' => 'commerce_boncard',
        'label' => $label,
        'amount' => $amount->multiply('-1'),
        'source_id' => $giftcard->id(),
      ]));

      $order_total = $order_total->subtract($amount);
    }
  }

}
