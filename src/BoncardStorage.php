<?php

namespace Drupal\commerce_boncard;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the boncard storage.
 */
class BoncardStorage extends CommerceContentEntityStorage implements BoncardStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByRemoteId($remote_id) {
    $boncards = $this->loadByProperties(['remote_id' => $remote_id]);
    $boncard = reset($boncards);

    return $boncard ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByOrder(OrderInterface $order) {
    $query = $this->getQuery()
      ->condition('order_id', $order->id())
      ->accessCheck(FALSE)
      ->sort('boncard_id');
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

}
