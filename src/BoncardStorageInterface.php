<?php

namespace Drupal\commerce_boncard;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for boncard transaction storage.
 */
interface BoncardStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads the boncard transaction for the given remote ID.
   *
   * @param string $remote_id
   *   The remote ID.
   *
   * @return \Drupal\commerce_boncard\BoncardInterface|null
   *   The boncard, or NULL if none found.
   */
  public function loadByRemoteId($remote_id);

  /**
   * Loads all boncard transactions for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_boncard\BoncardInterface[]
   *   The boncard transactions.
   */
  public function loadMultipleByOrder(OrderInterface $order);

}
