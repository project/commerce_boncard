<?php

namespace Drupal\commerce_boncard\Client;

use Drupal\commerce_boncard\BoncardInterface;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Language\LanguageManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 *
 */
class BoncardClient {

  /**
   * An http client factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Class responsible for providing language support on language-unaware sites.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $boncardStorage;

  /**
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The minor units converter.
   *
   * @var \Drupal\commerce_price\MinorUnitsConverterInterface
   */
  protected $minorUnitsConverter;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   */
  public function __construct(
    ClientFactory $httpClientFactory,
    RequestStack $request_stack,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    DateFormatterInterface $date_formatter,
    MinorUnitsConverterInterface $minor_units_converter,
    TimeInterface $time,
    LoggerInterface $logger,
  ) {
    $this->httpClientFactory = $httpClientFactory;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('commerce_boncard.settings');
    $this->boncardStorage = $this->entityTypeManager->getStorage('commerce_boncard');
    $this->dateFormatter = $date_formatter;
    $this->minorUnitsConverter = $minor_units_converter;
    $this->time = $time;
    $this->logger = $logger;
  }

  /**
   *
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client_factory'),
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('date.formatter'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('datetime.time'),
      $container->get('logger.channel.commerce_boncard')
    );
  }

  /**
   * Check balance remotely and create a local boncard transaction entity.
   *
   * @param $order_id
   * @param $card_number
   * @param $cvc
   *
   * @return \Drupal\commerce_boncard\BoncardInterface|null
   */
  public function checkBalance($order_id, $card_number, $cvc) {
    $card_number = str_replace(' ', '', $card_number);

    $parameters = [
      'userId' => $this->config->get('user_id'),
      'terminalId' => $this->config->get('terminal_id'),
      'cardNumber' => $card_number,
    ];

    $signature_payload = "BALANCE+{$this->config->get('terminal_id')}+{$card_number}";

    $response_data = $this->postRequest($parameters, '/api/v1/balance', $signature_payload);

    if ($this->validate($response_data, $card_number)) {
      return $this->getOrCreateBoncardTransaction($order_id, $card_number, $response_data['balance'], $cvc);
    }

    return NULL;
  }

  /**
   * This endpoint triggers a payment transaction and reduces the card balance by the amount transferred.
   * If the amount available is not sufficient, the transaction is rejected with the corresponding error code (NoFunds)l.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @return void
   */
  public function paymentRequest(BoncardInterface $boncard, Price $amount, $trxId) {
    $order = $boncard->getOrder();
    $parameters = [
      'userId' => $this->config->get('user_id'),
      'trxReferenceNr' => implode('_', [$trxId, $order->id()]),
      'dateYYYYMMDD' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Ymd'),
      'timeHHMMSS' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Hms'),
      'merchantName' => $this->config->get('merchant_name'),
      'merchantId' => $this->config->get('merchant_id'),
      'terminalId' => $this->config->get('terminal_id'),
      'cardNumber' => $boncard->getCardNumber(),
      'cvc' => $boncard->getCvc(),
      'amount' => $amount->getNumber(),
      'partialApproval' => FALSE,
      'currency' => $amount->getCurrencyCode(),
    ];

    $amountMinor = $this->minorUnitsConverter->toMinorUnits($amount);
    $signature_payload = "PAYMENT+{$parameters['trxReferenceNr']}+{$parameters['dateYYYYMMDD']}+{$parameters['timeHHMMSS']}+{$parameters['terminalId']}+{$parameters['cardNumber']}+{$amountMinor}";

    $response_data = $this->postRequest($parameters, '/api/v1/payment', $signature_payload);
    if ($this->validate($response_data, $boncard->getCardNumber())) {

      $boncard->setState('authorization');
      $boncard->setAmount($amount);
      // Update new card value.
      if ($response_data['balance']) {
        $boncard->setCardValue(new Price($response_data['balance'], 'CHF'));
      }
      $boncard->setExpiresTime(strtotime('+48 hour', $this->time->getRequestTime()));
      $boncard->save();
    }
  }

  /**
   * This endpoint confirms a previously authorized transaction.
   *
   * @param \Drupal\commerce_boncard\BoncardInterface $boncard
   * @param \Drupal\commerce_price\Price $amount
   *
   * @return void
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submissionRequest(BoncardInterface $boncard, Price $amount, $trxId) {
    $order = $boncard->getOrder();

    $parameters = [
      'userId' => $this->config->get('user_id'),
      'trxReferenceNr' => implode('_', [$trxId, $order->id()]),
      'dateYYYYMMDD' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Ymd'),
      'timeHHMMSS' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Hms'),
      'terminalId' => $this->config->get('terminal_id'),
      'cardNumber' => $boncard->getCardNumber(),
      'amount' => $amount->getNumber(),
      'currency' => $amount->getCurrencyCode(),
    ];

    $amountMinor = $this->minorUnitsConverter->toMinorUnits($amount);
    $signature_payload = "SUBMISSION+{$parameters['trxReferenceNr']}+{$parameters['dateYYYYMMDD']}+{$parameters['timeHHMMSS']}+{$parameters['terminalId']}+{$parameters['cardNumber']}+{$amountMinor}";

    $response_data = $this->postRequest($parameters, '/api/v1/submission', $signature_payload);
    if ($this->validate($response_data, $boncard->getCardNumber())) {

      // If this is a payment confirmation, set the status to complete.
      if (strpos($trxId, 'PAYMENT') === 0) {
        $boncard->setState('completed');
      }
      $boncard->setCompletedTime($this->time->getRequestTime());
      $boncard->save();
    }
  }

  /**
   * This endpoint triggers a credit transaction and increases the card balance by the amount transferred.
   * If the balance of the card account exceeds the assigned maximum value, the transaction is rejected with
   * a corresponding error code.
   *
   * @param \Drupal\commerce_boncard\BoncardInterface $boncard
   * @param \Drupal\commerce_price\Price $amount
   *
   * @return void
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function refund(BoncardInterface $boncard, Price $amount, $trxId) {
    $order = $boncard->getOrder();

    $parameters = [
      'userId' => $this->config->get('user_id'),
      'trxReferenceNr' => implode('_', [$trxId, $order->id()]),
      'dateYYYYMMDD' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Ymd'),
      'timeHHMMSS' => $this->dateFormatter->format($this->time->getRequestTime(), 'custom', 'Hms'),
      'merchantName' => $this->config->get('merchant_name'),
      'merchantId' => $this->config->get('merchant_id'),
      'terminalId' => $this->config->get('terminal_id'),
      'cardNumber' => $boncard->getCardNumber(),
      'cvc' => $boncard->getCvc(),
      'amount' => $amount->getNumber(),
      'currency' => $amount->getCurrencyCode(),
    ];

    $amountMinor = $this->minorUnitsConverter->toMinorUnits($amount);
    $signature_payload = "CREDIT+{$parameters['trxReferenceNr']}+{$parameters['dateYYYYMMDD']}+{$parameters['timeHHMMSS']}+{$parameters['terminalId']}+{$parameters['cardNumber']}+{$amountMinor}";

    $response_data = $this->postRequest($parameters, '/api/v1/credit', $signature_payload);

    if ($this->validate($response_data, $boncard->getCardNumber())) {
      // Save refunded amount.
      if ($boncard->getRefundedAmount() && $boncard->getRefundedAmount()->getNumber() > 0) {
        $amount = $amount->add($boncard->getRefundedAmount());
      }
      $boncard->setRefundedAmount($amount);
      $boncard->setState('refunded');
      if ($boncard->getBalance()->getNumber() > 0) {
        $boncard->setState('partially_refunded');
      }
      // Update new card value.
      if ($response_data['balance']) {
        $boncard->setCardValue(new Price($response_data['balance'], 'CHF'));
      }
      $boncard->save();
    }
  }

  /**
   * This endpoint cancels the transaction designated by the transaction reference number.
   *
   * @param \Drupal\commerce_boncard\BoncardInterface $boncard
   *
   * @return void
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function cancel(BoncardInterface $boncard, $trxId) {
    $order = $boncard->getOrder();

    $parameters = [
      'userId' => $this->config->get('user_id'),
      'trxReferenceNr' => implode('_', [$trxId, $order->id()]),
      'terminalId' => $this->config->get('terminal_id'),
    ];

    $signature_payload = "REVERSAL+{$parameters['trxReferenceNr']}+{$parameters['terminalId']}";

    $response_data = $this->postRequest($parameters, '/api/v1/reversal', $signature_payload);

    if ($this->validate($response_data, $boncard->getCardNumber())) {
      $boncard->setState('authorization_voided');
      $boncard->save();
    }
  }

  /**
   *
   */
  private function postRequest($parameters, $uri_path, $signature_payload) {
    // Add signature.
    $parameters['signature'] = $this->calculateSignature($signature_payload);

    $uri = $this->config->get('endpoint') . $uri_path;

    $httpClient = $this->httpClientFactory->fromOptions();
    $response = $httpClient->post(
      $uri,
      [
        'json' => $parameters,
      ]
    );

    $data = $response->getBody()->getContents();

    return json_decode($data, TRUE);
  }

  /**
   * Create a new boncard transaction or fetch an existing one of an order.
   *
   * @param $order_id
   * @param $card_number
   * @param $balance
   * @param $cvc
   *
   * @return \Drupal\commerce_boncard\BoncardInterface
   */
  private function getOrCreateBoncardTransaction($order_id, $card_number, $card_value = 0, $cvc = NULL) {
    // Find existing cards on the order.
    $giftcards = $this->boncardStorage->loadByProperties([
      'card_number' => $card_number,
      'order_id' => $order_id,
    ]);
    if (empty($giftcards)) {
      /** @var \Drupal\commerce_boncard\BoncardInterface $giftcard */
      $giftcard = $this->boncardStorage->create([
        'card_number' => $card_number,
        'cvc' => $cvc,
        'merchant_name' => $this->config->get('merchant_name'),
        'merchant_id' => $this->config->get('merchant_id'),
        'terminal_id' => $this->config->get('terminal_id'),
        'order_id' => $order_id,
        'card_value' => [
          'number' => $card_value,
          'currency_code' => 'CHF',
        ],
        'amount' => [
          'number' => 0,
          'currency_code' => 'CHF',
        ],
        'state' => 'new',
      ]);
    }
    else {
      /** @var \Drupal\commerce_boncard\BoncardInterface $giftcard */
      $giftcard = reset($giftcards);
    }

    $giftcard->save();
    return $giftcard;
  }

  /**
   * Calculate signature.
   *
   * @param $parameters
   *
   * @return string
   */
  private function calculateSignature($payload) {
    $payload = mb_convert_encoding($payload . '+' . $this->config->get('password'), 'UTF-8');
    $password = mb_convert_encoding($this->config->get('password'), 'UTF-8');
    $sha_mac = hash_hmac('sha256', $payload, $password);
    $signature = base64_encode(hex2bin($sha_mac));
    return $signature;
  }

  /**
   * Validate response.
   *
   * @param $response
   * @param $card_number
   *
   * @return bool|void
   */
  private function validate($response, $card_number) {
    if ($response && $response['status'] === 1) {
      return TRUE;
    }

    if ($response['extendedError'] !== 0) {
      $error = BoncardErrors::mapExtError($response['extendedError']);
      $this->logger->error(
        'Extended Error Code (@error) on card number @card_number',
        ['@error' => $error, 'card_number' => $card_number]
      );
    }

    if ($response['status'] !== 1) {
      $error_message = BoncardErrors::mapStatus($response['status']);
      throw new BoncardException($error_message);
    }
  }

}
