<?php

namespace Drupal\commerce_boncard\Client;

/**
 *
 */
final class BoncardErrors {

  /**
   *
   */
  public static function mapStatus($status) {
    switch ($status) {
      case 2:
        $message = t('Partial Approval');
        break;

      case 1:
        $message = t('OK');
        break;

      case 0:
        $message = t('Generic Error');
        break;

      case -1:
        $message = t('Card Unknown');
        break;

      case -2:
        $message = t('No Funds');
        break;

      case -3:
        $message = t('Authentication Error (Wrong CVC)');
        break;

      case -4:
        $message = t('Invalid Transaction');
        break;

      case -5:
        $message = t('Amount Invalid');
        break;

      case -6:
        $message = t('Card Locked');
        break;

      case -7:
        $message = t('Wrong Card Number');
        break;

      case -8:
        $message = t('Withdrawal Amount Exceed');
        break;

      case -9:
        $message = t('Card Expired');
        break;

      default:
        $message = t('Unknown status');
        break;
    }

    return $message;
  }

  /**
   *
   */
  public static function mapCardStatus($status) {
    switch ($status) {
      case -9:
        $message = t('Status Undefined');
        break;

      case -5:
        $message = t('Not Allowed');
        break;

      case -4:
        $message = t('Not Activated');
        break;

      case -3:
        $message = t('One Way Already Used');
        break;

      case -2:
        $message = t('Card Invalid');
        break;

      case -1:
        $message = t('Card Locked');
        break;

      case 0:
        $message = t('CardUnknown');
        break;

      case 1:
        $message = t('Card Active');
        break;

      default:
        $message = t('Unknown status');
        break;
    }

    return $message;
  }

  /**
   *
   */
  public static function mapExtError($error) {
    switch ($error) {
      case 0:
        $message = 'NOERROR';
        break;

      case -100:
        $message = 'EXTERROR_SYSTEM_DOWN';
        break;

      case -101:
        $message = 'EXTERROR_CARD_LOCKED';
        break;

      case -102:
        $message = 'EXTERROR_TERMINAL_UNKNOWN';
        break;

      case -103:
        $message = 'EXTERROR_FUNCTION_DISALLOWED';
        break;

      case -104:
        $message = 'EXTERROR_CARD_UNKNOWN';
        break;

      case -105:
        $message = 'EXTERROR_CVC_MISSMATCH';
        break;

      case -106:
        $message = 'EXTERROR_LOADREQUEST_INVALID';
        break;

      case -107:
        $message = 'EXTERROR_INVALID_AMOUNT';
        break;

      case -108:
        $message = 'EXTERROR_LOADREQUEST_OVERLOAD';
        break;

      case -109:
        $message = 'EXTERROR_GENERIC';
        break;

      case -110:
        $message = 'EXTERROR_INVALID_RANGE';
        break;

      case -111:
        $message = 'EXTERROR_NO_FUNDS';
        break;

      case -112:
        $message = 'EXTERROR_USEREQUEST_INVALID';
        break;

      case -113:
        $message = 'EXTERROR_CARD_DATE_EXCEEDED';
        break;

      case -114:
        $message = 'EXTERROR_SECURITY_ISSUE';
        break;

      case -115:
        $message = 'EXTERROR_INVALID_REQUEST';
        break;

      case -116:
        $message = 'EXTERROR_CARD_NOT_ACTIVATED';
        break;

      case -117:
        $message = 'EXTERROR_INVALID_CARDNUMBER';
        break;

      case -118:
        $message = 'EXTERROR_INVALID_SIGNATURE';
        break;

      case -119:
        $message = 'EXTERROR_INVALID_CONFIGURATION';
        break;

      case -120:
        $message = 'EXTERROR_INVALID_ORDERNUMBER';
        break;

      case -121:
        $message = 'EXTERROR_INVALID_ORDERITEMNUMBER';
        break;

      case -122:
        $message = 'EXTERROR_CARDRANGE_EXHAUSTED';
        break;

      case -123:
        $message = 'EXTERROR_FILE_NOT_FOUND';
        break;

      default:
        $message = 'Unknown status';
        break;
    }

    return $message;
  }

}
