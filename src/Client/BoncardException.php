<?php

namespace Drupal\commerce_boncard\Client;

/**
 * Base exception for all boncard errors.
 */
class BoncardException extends \RuntimeException {}
