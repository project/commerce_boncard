<?php

namespace Drupal\commerce_boncard;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the boncard transaction entity type.
 */
class CommerceBoncardAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view boncard transaction');

      case 'update':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'edit boncard transaction',
            'administer boncard transaction',
          ],
          'OR'
        );

      case 'delete':
        return AccessResult::allowedIfHasPermissions(
          $account,
          ['delete boncard transaction'],
        );

      case 'cancel':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'cancel boncard transaction',
            'administer boncard transaction',
          ],
          'OR'
        );

      case 'refund':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'refund boncard transaction',
            'administer boncard transaction',
          ],
          'OR'
        );

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      [
        'create boncard transaction',
        'administer boncard transaction',
      ],
      'OR'
    );
  }

}
