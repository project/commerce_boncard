<?php

namespace Drupal\commerce_boncard;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the boncard transaction entity type.
 */
class CommerceBoncardListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   * @param \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface $currency_formatter
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    DateFormatterInterface $date_formatter,
    RedirectDestinationInterface $redirect_destination,
    CurrencyFormatterInterface $currency_formatter,
    RouteMatchInterface $route_match,
  ) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->currencyFormatter = $currency_formatter;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('commerce_price.currency_formatter'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $order_id = $this->routeMatch->getRawParameter('commerce_order');
    $query = $this->getStorage()->getQuery()
      ->condition('order_id', $order_id)
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->accessCheck(FALSE)->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();

    $order_id = $this->routeMatch->getRawParameter('commerce_order');
    $query = $this->getStorage()->getQuery()
      ->condition('order_id', $order_id)
      ->sort($this->entityType->getKey('id'));

    $total = $query->count()
      ->accessCheck(FALSE)
      ->execute();

    $build['summary']['#markup'] = $this->t('Total commerce tickets: @total', ['@total' => $total]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['card_number'] = $this->t('Card Number');
    $header['status'] = $this->t('Status');
    $header['card_value'] = $this->t('Card Value');
    $header['amount'] = $this->t('Amount');
    $header['balance'] = $this->t('Balance');
    $header['refunded'] = $this->t('Refunded');
    $header['expires'] = $this->t('Expires');
    $header['completed'] = $this->t('Completed');
    $header['changed'] = $this->t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_boncard\BoncardInterface $entity */
    $row['id'] = $entity->id();
    $row['card_number'] = $entity->getCardNumber();
    $row['status'] = $entity->getState()->getLabel();
    $row['card_value'] = $entity->getCardValue() ? $this->currencyFormatter->format(
      $entity->getCardValue()->getNumber(),
      $entity->getCardValue()->getCurrencyCode()
    ) : 0;
    $row['amount'] = $entity->getAmount() ? $this->currencyFormatter->format(
      $entity->getAmount()->getNumber(),
      $entity->getAmount()->getCurrencyCode()
    ) : 0;
    $row['balance'] = $entity->getBalance() ? $this->currencyFormatter->format(
      $entity->getBalance()->getNumber(),
      $entity->getBalance()->getCurrencyCode()
    ) : 0;
    $row['refunded'] = $entity->getRefundedAmount() ? $this->currencyFormatter->format(
      $entity->getRefundedAmount()->getNumber(),
      $entity->getRefundedAmount()->getCurrencyCode()
    ) : 0;
    $row['expires'] = $entity->getExpiresTime() ? $this->dateFormatter->format($entity->getExpiresTime(), 'short') : '';
    $row['completed'] = $entity->getCompletedTime() ? $this->dateFormatter->format($entity->getCompletedTime(), 'short') : '';
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime(), 'short');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = $entity->buildBoncardOperations();
    // Filter out operations that aren't allowed.
    $operations = array_filter($operations, function ($operation) {
      return !empty($operation['access']);
    });
    // Build the url for each operation.
    $base_route_parameters = [
      'commerce_boncard' => $entity->id(),
      'commerce_order' => $entity->getOrderId(),
    ];
    foreach ($operations as $operation_id => $operation) {
      $route_parameters = $base_route_parameters + ['operation' => $operation_id];
      $operation['url'] = new Url("entity.commerce_boncard.operation", $route_parameters);
      $operations[$operation_id] = $operation;
    }

    if ($entity->access('update') && $entity->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('edit-form')->setRouteParameter('commerce_order', $entity->getOrderId())),
      ];
    }
    if ($entity->access('delete') && $entity->hasLinkTemplate('delete-form')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 100,
        'url' => $this->ensureDestination($entity->toUrl('delete-form')->setRouteParameter('commerce_order', $entity->getOrderId())),
      ];
    }

    $redirect = Url::fromRoute('entity.commerce_boncard.collection', ['commerce_order' => $entity->getOrderId()])->toString();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query']['destination'] = $redirect;
    }
    return $operations;
  }

}
