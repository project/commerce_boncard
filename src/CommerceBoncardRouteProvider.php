<?php

namespace Drupal\commerce_boncard;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for the Boncard entity.
 */
class CommerceBoncardRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddFormRoute($entity_type);
    if ($route) {
      $route->setOption(
        'parameters',
        [
          'commerce_order' => [
            'type' => 'entity:commerce_order',
          ],
        ]
      );
    }
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCanonicalRoute($entity_type);
    // $route = new Route($entity_type->getLinkTemplate('canonical'));
    if ($route) {
      $route->setOption(
        'parameters',
        [
          'commerce_order' => [
            'type' => 'entity:commerce_order',
          ],
        ]
      );
    }
    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);

    // Add overview permission.
    $overview_permission = "access boncard transaction overview";
    $route->setRequirement('_permission', "$overview_permission");

    if ($route) {
      $route->setOption(
        'parameters',
        [
          'commerce_order' => [
            'type' => 'entity:commerce_order',
          ],
        ]
      );
    }
    return $route;
  }

}
