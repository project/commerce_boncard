<?php

namespace Drupal\commerce_boncard\Entity;

use Drupal\commerce_boncard\BoncardInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Defines the boncard transaction entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_boncard",
 *   label = @Translation("Boncard Transaction"),
 *   label_collection = @Translation("Boncard Transactions"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "event" = "Drupal\commerce_boncard\Event\BoncardEvent",
 *     "list_builder" = "Drupal\commerce_boncard\CommerceBoncardListBuilder",
 *     "views_data" = "Drupal\commerce\CommerceEntityViewsData",
 *     "access" = "Drupal\commerce_boncard\CommerceBoncardAccessControlHandler",
 *     "storage" = "Drupal\commerce_boncard\BoncardStorage",
 *     "form" = {
 *       "operation" = "Drupal\commerce_boncard\Form\BoncardOperationForm",
 *       "add" = "Drupal\commerce_boncard\Form\BoncardForm",
 *       "edit" = "Drupal\commerce_boncard\Form\BoncardForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_boncard\CommerceBoncardRouteProvider",
 *     }
 *   },
 *   base_table = "commerce_boncard",
 *   admin_permission = "administer boncard transaction",
 *   entity_keys = {
 *     "id" = "boncard_id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   field_indexes = {
 *     "remote_id"
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/orders/{commerce_order}/giftcards/add",
 *     "edit-form" = "/admin/commerce/orders/{commerce_order}/giftcards/{commerce_boncard}/edit",
 *     "delete-form" = "/admin/commerce/orders/{commerce_order}/giftcards/{commerce_boncard}/delete",
 *     "collection" = "/admin/commerce/orders/{commerce_order}/giftcards"
 *   },
 *   field_ui_base_route = "commerce_boncard.settings_form"
 * )
 */
class Boncard extends ContentEntityBase implements BoncardInterface {

  use EntityChangedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label() {
    // UIs should use the number formatter to show a more user-readable version.
    return $this->getAmount()->__toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->get('order_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId() {
    return $this->get('remote_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteId($remote_id) {
    $this->set('remote_id', $remote_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteState() {
    return $this->get('remote_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteState($remote_state) {
    $this->set('remote_state', $remote_state);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBalance() {
    if ($amount = $this->getAmount()) {
      $balance = $amount;
      if ($refunded_amount = $this->getRefundedAmount()) {
        $balance = $balance->subtract($refunded_amount);
      }
      return $balance;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCardValue() {
    if (!$this->get('card_value')->isEmpty()) {
      return $this->get('card_value')->first()->toPrice();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCardValue(Price $amount) {
    $this->set('card_value', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmount() {
    if (!$this->get('amount')->isEmpty()) {
      return $this->get('amount')->first()->toPrice();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAmount(Price $amount) {
    $this->set('amount', $amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefundedAmount() {
    if (!$this->get('refunded_amount')->isEmpty()) {
      return $this->get('refunded_amount')->first()->toPrice();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setRefundedAmount(Price $refunded_amount) {
    $this->set('refunded_amount', $refunded_amount);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function setState($state_id) {
    $this->set('state', $state_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizedTime() {
    return $this->get('authorized')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAuthorizedTime($timestamp) {
    $this->set('authorized', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpired() {
    $expires = $this->getExpiresTime();
    return $expires > 0 && $expires <= \Drupal::time()->getRequestTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiresTime() {
    return $this->get('expires')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExpiresTime($timestamp) {
    $this->set('expires', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isCompleted() {
    return !$this->get('completed')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getCompletedTime() {
    return $this->get('completed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCompletedTime($timestamp) {
    $this->set('completed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCardNumber() {
    return $this->get('card_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCardNumber($card_number) {
    $this->set('card_number', $card_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCvc() {
    return $this->get('cvc')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCvc($cvc) {
    $this->set('cvc', $cvc);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMerchantName() {
    return $this->get('merchant_name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMerchantName($merchant_name) {
    $this->set('merchant_name', $merchant_name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMechantId() {
    return $this->get('merchant_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMerchantId($merchant_id) {
    $this->set('merchant_id', $merchant_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTerminalId() {
    return $this->get('terminal_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTerminalId($terminal_id) {
    $this->set('terminal_id', $terminal_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['card_number'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Card Number'))
      ->setDescription(t('The card number.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['cvc'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('CVC'))
      ->setDescription(t('The card CVC.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['merchant_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Merchant Name'))
      ->setDescription(t('The merchant name.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['merchant_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Merchant ID'))
      ->setDescription(t('The merchant id.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['terminal_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Terminal ID'))
      ->setDescription(t('The terminal id.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'string_textfield',
          'weight' => -4,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order', [], ['context' => 'commerce_boncard']))
      ->setDescription(t('The parent order.'))
      ->setSetting('target_type', 'commerce_order')
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['remote_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote ID'))
      ->setDescription(t('The remote boncard transaction ID.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', FALSE);

    $fields['remote_state'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Remote State'))
      ->setDescription(t('The remote boncard transaction state.'))
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('view', FALSE);

    $fields['card_value'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Card Value'))
      ->setDescription(t('The boncard card value.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'commerce_unit_price',
        'weight' => 2,
        'settings' => [
          'require_confirmation' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Amount'))
      ->setDescription(t('The boncard transaction amount.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'commerce_unit_price',
        'weight' => 2,
        'settings' => [
          'require_confirmation' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['refunded_amount'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('Refunded amount'))
      ->setDescription(t('The refunded boncard transaction amount.'))
      ->setDisplayOptions('form', [
        'type' => 'commerce_unit_price',
        'weight' => 2,
        'settings' => [
          'require_confirmation' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('The boncard transaction state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setSetting(
        'workflow_callback',
        ['\Drupal\commerce_boncard\Entity\Boncard', 'getWorkflowId']
      );

    $fields['authorized'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Authorized'))
      ->setDescription(t('The time when the boncard transaction was authorized.'))
      ->setDisplayConfigurable('view', FALSE);

    $fields['expires'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expires'))
      ->setDescription(t('The time when the transaction expires. 0 for never.'))
      ->setDisplayConfigurable('view', FALSE)
      ->setDefaultValue(0);

    $fields['completed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Completed'))
      ->setDescription(t('The time when the transaction was completed.'))
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the boncard transaction was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', FALSE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the boncard transaction was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function buildBoncardOperations() {
    $payment_state = $this->getState()->getId();
    $operations = [];
    $operations['cancel'] = [
      'title' => $this->t('Cancel transaction'),
      'page_title' => $this->t('Cancel giftcard payment'),
      'plugin_form' => 'cancel-boncard',
      'access' => $payment_state == 'authorization',
    ];
    $operations['refund'] = [
      'title' => $this->t('Refund transaction'),
      'page_title' => $this->t('Refund giftcard payment'),
      'plugin_form' => 'refund-boncard',
      'access' => in_array($payment_state, ['completed', 'partially_refunded']),
    ];

    return $operations;
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\commerce_boncard\BoncardInterface $boncard
   *   The boncard transaction.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(BoncardInterface $boncard) {
    return 'boncard_default';
  }

}
