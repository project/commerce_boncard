<?php

namespace Drupal\commerce_boncard\Event;

use Drupal\commerce\EventBase;
use Drupal\commerce_boncard\BoncardInterface;

/**
 * Defines the boncard event.
 *
 * @see \Drupal\commerce_boncard\Event\BoncardEvents
 */
class BoncardEvent extends EventBase {

  /**
   * The boncard.
   *
   * @var \Drupal\commerce_boncard\BoncardInterface
   */
  protected $boncard;

  /**
   * Constructs a new BoncardEvent.
   *
   * @param \Drupal\commerce_boncard\BoncardInterface $boncard
   *   The boncard transaction.
   */
  public function __construct(BoncardInterface $boncard) {
    $this->boncard = $boncard;
  }

  /**
   * Gets the boncard transaction.
   *
   * @return \Drupal\commerce_boncard\BoncardInterface
   *   The boncard transaction.
   */
  public function getBoncard() {
    return $this->boncard;
  }

}
