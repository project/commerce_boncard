<?php

namespace Drupal\commerce_boncard\Event;

/**
 *
 */
final class BoncardEvents {

  /**
   * Name of the event fired after loading a boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_LOAD = 'commerce_boncard.commerce_boncard.load';

  /**
   * Name of the event fired after creating a new boncard transaction.
   *
   * Fired before the boncard transaction is saved.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_CREATE = 'commerce_boncard.commerce_boncard.create';

  /**
   * Name of the event fired before saving a boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_PRESAVE = 'commerce_boncard.commerce_boncard.presave';

  /**
   * Name of the event fired after saving a new boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_INSERT = 'commerce_boncard.commerce_boncard.insert';

  /**
   * Name of the event fired after saving an existing boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_UPDATE = 'commerce_boncard.commerce_boncard.update';

  /**
   * Name of the event fired before deleting a boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_PREDELETE = 'commerce_boncard.commerce_boncard.predelete';

  /**
   * Name of the event fired after deleting a boncard transaction.
   *
   * @Event
   *
   * @see \Drupal\commerce_boncard\Event\BoncardEvents
   */
  const BONCARD_DELETE = 'commerce_boncard.commerce_boncard.delete';

}
