<?php

namespace Drupal\commerce_boncard\EventSubscriber;

use Drupal\commerce_boncard\BoncardInterface;
use Drupal\commerce_boncard\Client\BoncardClient;
use Drupal\commerce_boncard\Client\BoncardException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Order event subscriber.
 */
class OrderEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\commerce_boncard\Client\BoncardClient
   */
  protected $client;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manages entity type plugin definitions.
   * @param \Drupal\commerce_boncard\Client\BoncardClient $client
   * @param \Psr\Log\LoggerInterface $logger
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, BoncardClient $client, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->client = $client;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Run very late to kick in atfer payment subscribers.
    $events = [
      'commerce_order.place.post_transition' => ['orderPlaced', -1000],
    ];
    return $events;
  }

  /**
   * Listens on the order placed event.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The workflow transition event.
   */
  public function orderPlaced(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    // Don't send boncard calls if the order is not paid yet.
    if (!$order->isPaid()) {
      return;
    }
    $adjustments = $order->collectAdjustments();
    foreach ($adjustments as $adjustment) {
      if ($adjustment->getType() != 'commerce_boncard' || !$adjustment->getSourceId() || $adjustment->getAmount()->isZero()) {
        continue;
      }

      $gift_card = $this->entityTypeManager->getStorage('commerce_boncard')->load($adjustment->getSourceId());

      // Load and settle the giftcard amount.
      if ($gift_card instanceof BoncardInterface) {

        // Settle the amount because else it gets automatically deleted after 48 hours.
        if ($gift_card->getState()->value === 'authorization') {
          try {
            $trxId = 'PAYMENT' . $gift_card->id();
            $amount = $adjustment->getAmount()->multiply('-1');
            $this->client->submissionRequest($gift_card, $amount, $trxId);
            $this->logger->info('Giftcard @card was successfully settled for order @order with amount @amount.',
              [
                '@card' => $gift_card->getCardNumber(),
                '@order' => $order->id(),
                '@amount' => $amount->__toString(),
              ]
            );
          }
          catch (BoncardException $exception) {
            $this->logger->critical('Giftcard @card on order @order with amount @amount could not be settled.',
              [
                '@card' => $gift_card->getCardNumber(),
                '@order' => $order->id(),
                '@amount' => $amount->__toString(),
              ]
            );
          }
        }
      }
    }
  }

}
