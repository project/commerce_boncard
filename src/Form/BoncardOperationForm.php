<?php

namespace Drupal\commerce_boncard\Form;

use Drupal\commerce\InlineFormManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the commerce_boncard transaction operation form.
 */
class BoncardOperationForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * Constructs a new PaymentAddForm instance.
   *
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   */
  public function __construct(InlineFormManager $inline_form_manager) {
    $this->inlineFormManager = $inline_form_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_inline_form')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_boncard\Entity\Boncard $boncard */
    $boncard = $this->entity;
    $operations = $boncard->buildBoncardOperations();
    $operation_id = $this->getRouteMatch()->getParameter('operation');
    $operation = $operations[$operation_id];

    $inline_form = $this->inlineFormManager->createInstance("commerce_boncard.{$operation_id}", [
      'operation' => $operation['plugin_form'],
    ], $this->entity);

    $form['#title'] = $operation['page_title'];
    $form['#tree'] = TRUE;
    $form['boncard'] = [
      '#parents' => ['boncard'],
      '#inline_form' => $inline_form,
    ];
    $form['boncard'] = $inline_form->buildInlineForm($form['boncard'], $form_state);
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $operation['title'],
      '#button_type' => 'primary',
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => $this->entity->toUrl('collection')->setRouteParameter('commerce_order', $boncard->getOrderId()),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    $inline_form = $form['boncard']['#inline_form'];
    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $inline_form->getEntity();

    if (!empty($form['boncard']['#success_message'])) {
      $this->messenger()->addMessage($form['boncard']['#success_message']);
    }
    $form_state->setRedirect('entity.commerce_boncard.collection', ['commerce_order' => $boncard->getOrderId()]);
  }

}
