<?php

namespace Drupal\commerce_boncard\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure commerce_boncard settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_boncard_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_boncard.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Endpoint Boncard'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('endpoint') ?? 'https://test-paymentservice.boncard.ch',
      '#description' => $this->t('Can be overriden via settings.php with per environment.'),
      '#required' => TRUE,
    ];
    $form['merchant_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Name'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('merchant_name'),
      '#required' => TRUE,
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Id'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('merchant_id'),
      '#required' => TRUE,
    ];
    $form['terminal_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terminal Id'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('terminal_id'),
      '#required' => TRUE,
    ];
    $form['user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Id'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('user_id'),
      '#required' => TRUE,
    ];
    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password (for signature)'),
      '#default_value' => $this->config('commerce_boncard.settings')->get('password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!UrlHelper::isValid($form_state->getValue('endpoint'), TRUE)) {
      $form_state->setErrorByName('endpoint', $this->t('The URL is not valid.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_boncard.settings')
      ->set('endpoint', rtrim($form_state->getValue('endpoint'), '/\\'))
      ->set('user_id', $form_state->getValue('user_id'))
      ->set('merchant_name', $form_state->getValue('merchant_name'))
      ->set('merchant_id', $form_state->getValue('merchant_id'))
      ->set('terminal_id', $form_state->getValue('terminal_id'))
      ->save();

    if (!empty($form_state->getValue('password'))) {
      $this->config('commerce_boncard.settings')
        ->set('password', $form_state->getValue('password'))
        ->save();
    }

    parent::submitForm($form, $form_state);
  }

}
