<?php

namespace Drupal\commerce_boncard\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_boncard\BoncardInterface;
use Drupal\commerce_boncard\Client\BoncardClient;
use Drupal\commerce_boncard\Client\BoncardException;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the payment process pane.
 *
 * @CommerceCheckoutPane(
 *   id = "boncard_process",
 *   label = @Translation("Boncard processing"),
 *   default_step = "payment",
 *   wrapper_element = "container",
 * )
 */
class BoncardProcess extends CheckoutPaneBase {

  /**
   * @var \Drupal\commerce_boncard\Client\BoncardClient
   */
  protected $client;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected $inlineFormManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new PaymentProcess object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CheckoutFlowInterface $checkout_flow,
    EntityTypeManagerInterface $entity_type_manager,
    InlineFormManager $inline_form_manager,
    LoggerInterface $logger,
    BoncardClient $client,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->inlineFormManager = $inline_form_manager;
    $this->logger = $logger;
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
    CheckoutFlowInterface $checkout_flow = NULL,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('logger.channel.commerce_boncard'),
      $container->get('commerce_boncard.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    $adjustments = $this->order->collectAdjustments();
    foreach ($adjustments as $adjustment) {
      if ($adjustment->getType() === 'commerce_boncard' && $adjustment->getSourceId() && !$adjustment->getAmount()
        ->isZero()) {
        // Check if we already have processed the gift card.
        $gift_card = $this->entityTypeManager->getStorage('commerce_boncard')
          ->load($adjustment->getSourceId());
        if ($gift_card->getState()->value === 'new') {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $error_step_id = $this->getErrorStepId();
    $next_step_id = $this->checkoutFlow->getNextStepId($this->getStepId());

    $adjustments = $this->order->collectAdjustments();
    foreach ($adjustments as $adjustment) {
      if ($adjustment->getType() != 'commerce_boncard' || !$adjustment->getSourceId() || $adjustment->getAmount()
        ->isZero()) {
        continue;
      }

      $gift_card = $this->entityTypeManager->getStorage('commerce_boncard')
        ->load($adjustment->getSourceId());

      // Load and settle the giftcard amount.
      if ($gift_card instanceof BoncardInterface) {
        try {

          // Authorize the amount.
          // The settlement will be done as soon as the order was placed successfully.
          if ($gift_card->getState()->value === 'new') {
            $trxId = 'PAYMENT' . $gift_card->id();
            $amount = $adjustment->getAmount()->multiply('-1');
            $this->client->paymentRequest($gift_card, $amount, $trxId);
            $this->logger->info('Giftcard @card was successfully authorized for order @order with amount @amount.',
              [
                '@card' => $gift_card->getCardNumber(),
                '@order' => $this->order->id(),
                '@amount' => $amount->__toString(),
              ]
            );
          }

        }
        catch (BoncardException $exception) {
          $message = $this->t(
            'We encountered an error processing your gift card: @message. Please verify your details and try again.',
            [
              '@message' => $exception->getMessage(),
            ]
                  );
          $this->messenger()->addError($message);
          $this->logger->error('Giftcard @card could not be authorized for order @order with amount @amount.',
                    [
                      '@card' => $gift_card->getCardNumber(),
                      '@order' => $this->order->id(),
                      '@amount' => $amount->__toString(),
                    ]
                  );
          $this->checkoutFlow->redirectToStep($error_step_id);
        }
      }
    }

    // If the order was paid in full with a gift card.
    if ($this->order->isPaid() || $this->order->getTotalPrice()->isZero()) {
      $this->checkoutFlow->redirectToStep($next_step_id);
    }

    // Redirect to payment again to do a normal payment.
    $this->checkoutFlow->redirectToStep('payment');

    $pane_form['place_holder'] = [
      '#type' => 'hidden',
      '#value' => '',
    ];
    return $pane_form;
  }

  /**
   * Gets the step ID that the customer should be sent to on error.
   *
   * @return string
   *   The error step ID.
   */
  protected function getErrorStepId() {
    // Default to the step that contains the PaymentInformation pane.
    $step_id = $this->checkoutFlow->getPane('commerce_boncard_redemption')
      ->getStepId();
    if ($step_id == '_disabled') {
      // Can't redirect to the _disabled step. This could mean that isVisible()
      // was overridden to allow PaymentProcess to be used without a
      // payment_information pane, but this method was not modified.
      throw new \RuntimeException('Cannot get the step ID for the payment_information pane. The pane is disabled.');
    }

    return $step_id;
  }

}
