<?php

namespace Drupal\commerce_boncard\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\commerce_boncard\Client\BoncardClient;
use Drupal\commerce_boncard\Client\BoncardException;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the boncard transaction cancel form.
 *
 * @CommerceInlineForm(
 *   id = "commerce_boncard.cancel",
 *   label = @Translation("Giftcard cancel form"),
 * )
 */
class BoncardCancelForm extends EntityInlineFormBase {

  /**
   * @var \Drupal\commerce_boncard\Client\BoncardClient
   */
  protected $client;

  /**
   * Constructs a new InlineFormBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BoncardClient $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_boncard.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $this->entity;

    $inline_form['#theme'] = 'confirm_form';
    $inline_form['#attributes']['class'][] = 'confirmation';
    $inline_form['#page_title'] = $this->t('Are you sure you want to cancel and refund the boncard transaction with amount %label?', [
      '%label' => $boncard->label(),
    ]);
    $inline_form['#success_message'] = $this->t('Boncard transaction cancelled.');
    $inline_form['description'] = [
      '#markup' => $this->t('This action cannot be undone.'),
    ];

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $this->entity;

    try {
      $trxId = 'CANCEL' . $boncard->id();
      $this->client->cancel($boncard, $trxId);
    }
    catch (BoncardException $e) {
      unset($inline_form['#success_message']);
      $this->messenger()->addError($e->getMessage());
    }
  }

}
