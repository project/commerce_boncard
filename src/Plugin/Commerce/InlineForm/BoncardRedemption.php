<?php

namespace Drupal\commerce_boncard\Plugin\Commerce\InlineForm;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormBase;
use Drupal\commerce_boncard\BoncardInterface;
use Drupal\commerce_boncard\Client\BoncardClient;
use Drupal\commerce_boncard\Client\BoncardException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for redeeming a boncard.
 *
 * @CommerceInlineForm(
 *   id = "commerce_boncard_redemption",
 *   label = @Translation("Giftcard redemption"),
 * )
 */
class BoncardRedemption extends InlineFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\commerce_boncard\Client\BoncardClient
   */
  protected $boncardClient;

  /**
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected $currencyFormatter;

  /**
   * Constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Manages entity type plugin definitions.
   * @param \Drupal\commerce_boncard\Client\BoncardClient $boncard_client
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    BoncardClient $boncard_client,
    CurrencyFormatterInterface $currency_formatter,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->boncardClient = $boncard_client;
    $this->currencyFormatter = $currency_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_boncard.client'),
      $container->get('commerce_price.currency_formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The order_id is passed via configuration to avoid serializing the
      // order, which is loaded from scratch in the submit handler to minimize
      // chances of a conflicting save.
      'order_id' => '',
      'max_giftcards' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function requiredConfiguration() {
    return ['order_id'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    $order = $this->entityTypeManager->getStorage('commerce_order')->load($this->configuration['order_id']);
    if (!$order) {
      throw new \RuntimeException('Invalid order_id given to the boncard_redemption inline form.');
    }
    assert($order instanceof OrderInterface);

    /** @var \Drupal\commerce_boncard\BoncardStorageInterface $boncard_storage */
    $boncard_storage = $this->entityTypeManager->getStorage('commerce_boncard');

    /** @var \Drupal\commerce_boncard\BoncardInterface $giftcards */
    $giftcards = $boncard_storage->loadMultipleByOrder($order);

    $inline_form = [
      '#tree' => TRUE,
      '#configuration' => $this->getConfiguration(),
    ] + $inline_form;

    $inline_form['card_number'] = [

      '#type' => 'textfield',
      '#placeholder' => $this->t('Gift card number'),
      // Chrome autofills this field with the address line 1, and ignores
      // autocomplete => 'off', but respects 'new-password'.
      '#attributes' => [
        'autocomplete' => 'nope',
      ],
    ];
    $inline_form['cvc'] = [
      '#type' => 'textfield',
      '#placeholder' => $this->t('CVC'),
      '#attributes' => [
        'autocomplete' => 'nope',
      ],
      '#size' => 4,
      '#maxlength' => 4,
    ];
    $inline_form['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#name' => 'apply_giftcard',
      '#limit_validation_errors' => [
        $inline_form['#parents'],
      ],
      // '#limit_validation_errors' => [['card_number']],
      '#submit' => [
        [get_called_class(), 'applyGiftcard'],
      ],
      '#ajax' => [
        'callback' => [get_called_class(), 'ajaxRefreshForm'],
        'element' => $inline_form['#parents'],
      ],
    ];
    $max_giftcards = $this->configuration['max_giftcards'];
    if ($max_giftcards && count($giftcards) >= $max_giftcards) {
      // Don't allow additional gift cards to be added.
      $inline_form['card_number']['#access'] = FALSE;
      $inline_form['cvc']['#access'] = FALSE;
      $inline_form['apply']['#access'] = FALSE;
    }

    /** * @var  \Drupal\commerce_boncard\BoncardInterface $giftcard */
    foreach ($giftcards as $index => $giftcard) {
      $inline_form['giftcards'][$index]['card_number'] = [
        '#type' => 'item',
        '#markup' => new FormattableMarkup('<span class="card_number">@card</span>', ['@card' => $giftcard->getCardNumber()]),
      ];

      /** @var \Drupal\commerce_price\Price $price */
      $card_value = $giftcard->getCardValue();
      $formatted_price = $this->currencyFormatter->format($card_value->getNumber(), $card_value->getCurrencyCode());

      $new_amount = $card_value->add($this->findAdjustment($order, $giftcard));
      $new_formatted_price = $this->currencyFormatter->format($new_amount->getNumber(), $new_amount->getCurrencyCode());

      $price_output = $this->t('The current balance is @amount. The new balance will be @new_amount.', [
        '@amount' => $formatted_price,
        '@new_amount' => $new_formatted_price,
      ]);

      $inline_form['giftcards'][$index]['balance'] = [
        '#type' => 'item',
        '#markup' => $price_output,
      ];

      $inline_form['giftcards'][$index]['remove_button'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove gift card'),
        '#name' => 'remove_giftcard_' . $index,
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefreshForm'],
          'element' => $inline_form['#parents'],
        ],
        '#weight' => 50,
        '#limit_validation_errors' => [
          $inline_form['#parents'],
        ],
        '#boncard_id' => $giftcard->id(),
        '#submit' => [
          [get_called_class(), 'removeGiftcard'],
        ],
        // Simplify ajaxRefresh() by having all triggering elements
        // on the same level.
        '#parents' => array_merge($inline_form['#parents'], ['remove_giftcard_' . $index]),
      ];
    }

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::validateInlineForm($inline_form, $form_state);

    // Runs if the 'Apply gift card' button was clicked, or the main form
    // was submitted by the user clicking the primary submit button.
    $triggering_element = $form_state->getTriggeringElement();
    $button_type = $triggering_element['#button_type'] ?? NULL;
    if ($triggering_element['#name'] != 'apply_giftcard' && $button_type != 'primary') {
      return;
    }

    $abort = FALSE;

    // Validate card number presence.
    $giftcard_code_parents = array_merge($inline_form['#parents'], ['card_number']);
    $giftcard_code = $form_state->getValue($giftcard_code_parents);
    $giftcard_code_path = implode('][', $giftcard_code_parents);
    if (empty($giftcard_code)) {
      if ($triggering_element['#name'] == 'apply_giftcard') {
        $form_state->setErrorByName($giftcard_code_path, $this->t('Please provide a gift card number.'));
      }
      $abort = TRUE;
    }

    // Validate cvc code presence.
    $giftcard_cvc_parents = array_merge($inline_form['#parents'], ['cvc']);
    $giftcard_cvc = $form_state->getValue($giftcard_cvc_parents);
    $giftcard_cvc_path = implode('][', $giftcard_cvc_parents);
    if (empty($giftcard_cvc)) {
      if ($triggering_element['#name'] == 'apply_giftcard') {
        $form_state->setErrorByName($giftcard_cvc_path, $this->t('Please provide the 3-digit CVC number.'));
        $abort = TRUE;
      }
    }

    if ($abort) {
      return;
    }

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $order_storage->load($this->configuration['order_id']);

    /** @var \Drupal\commerce_boncard\BoncardInterface $giftcard */
    try {
      $giftcard = $this->boncardClient->checkBalance($this->configuration['order_id'], $giftcard_code, $giftcard_cvc);
    }
    catch (BoncardException $exception) {
      $form_state->setErrorByName($giftcard_code_path, $exception->getMessage());
      return;
    }

    // Check balance.
    if ($giftcard->getCardValue()->isZero()) {
      $form_state->setErrorByName($giftcard_code_path, $this->t('The provided gift card has no balance.'));
      return;
    }

    // Verify currency.
    if ($giftcard->getCardValue()->getCurrencyCode() !== $order->getTotalPrice()->getCurrencyCode()) {
      $form_state->setErrorByName(
        $giftcard_code_path,
        $this->t('The order currency (%order_currency) does not match the giftcard currency (%giftcard_currency).', [
          '%order_currency' => $order->getTotalPrice()->getCurrencyCode(),
          '%giftcard_currency' => $giftcard->getCardValue()->getCurrencyCode(),
        ])
      );
      return;
    }

    // Save the gift card ID for applyGiftcard.
    $inline_form['card_number']['#boncard_id'] = $giftcard->id();
  }

  /**
   * Submit callback for the "Apply gift card" button.
   */
  public static function applyGiftcard(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = array_slice($triggering_element['#parents'], 0, -1);
    $inline_form = NestedArray::getValue($form, $parents);
    // Clear the gift card code input.
    $user_input = &$form_state->getUserInput();
    NestedArray::setValue($user_input, array_merge($parents, ['card_number']), '');
    NestedArray::setValue($user_input, array_merge($parents, ['cvc']), '');

    // Resave the order to trigger a recalculation.
    if (isset($inline_form['card_number']['#boncard_id'])) {
      $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $order_storage->load($inline_form['#configuration']['order_id']);
      $order->save();
    }

    $form_state->setRebuild();
  }

  /**
   * Submit callback for the "Remove gift card" button.
   */
  public static function removeGiftcard(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $parents = array_slice($triggering_element['#parents'], 0, -1);
    $inline_form = NestedArray::getValue($form, $parents);

    $boncard_storage = \Drupal::entityTypeManager()->getStorage('commerce_boncard');
    if ($giftcard_id = $triggering_element['#boncard_id']) {
      $giftcard = $boncard_storage->load($giftcard_id);
      if ($giftcard) {
        $boncard_storage->delete([$giftcard]);
      }
    }

    // Resave the order to trigger a recalculation.
    $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $order_storage->load($inline_form['#configuration']['order_id']);
    $order->save();
    $form_state->setRebuild();
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Drupal\commerce_boncard\BoncardInterface $giftcard
   *
   * @return \Drupal\commerce_price\Price
   */
  private function findAdjustment(OrderInterface $order, BoncardInterface $giftcard) {
    $adjustments = $order->getAdjustments(['commerce_boncard']);
    foreach ($adjustments as $adjustment) {

      if ($adjustment->getSourceId() === $giftcard->id()) {
        return $adjustment->getAmount();
      }
    }

    return new Price(0, 'CHF');
  }

}
