<?php

namespace Drupal\commerce_boncard\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\commerce_boncard\Client\BoncardClient;
use Drupal\commerce_boncard\Client\BoncardException;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the boncard transaction cancel form.
 *
 * @CommerceInlineForm(
 *   id = "commerce_boncard.refund",
 *   label = @Translation("Giftcard refund form"),
 * )
 */
class BoncardRefundForm extends EntityInlineFormBase {

  /**
   * @var \Drupal\commerce_boncard\Client\BoncardClient
   */
  protected $client;

  /**
   * Constructs a new InlineFormBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, BoncardClient $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_boncard.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);
    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $this->entity;

    $inline_form['#success_message'] = $this->t('Boncard transaction refunded.');
    $inline_form['amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Amount'),
      '#default_value' => $boncard->getBalance()->toArray(),
      '#required' => TRUE,
      '#available_currencies' => [$boncard->getAmount()->getCurrencyCode()],
    ];

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::validateInlineForm($inline_form, $form_state);
    $values = $form_state->getValue($inline_form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $this->entity;
    $balance = $boncard->getBalance();
    if ($amount->greaterThan($balance)) {
      $error_message = $this->t("Can't refund more than @amount.", ['@amount' => $balance->__toString()]);
      $this->messenger()->addError($error_message);
      $form_state->setError($inline_form['amount'], $error_message);
    }
  }

  /**
   *
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    $values = $form_state->getValue($inline_form['#parents']);
    $amount = Price::fromArray($values['amount']);
    /** @var \Drupal\commerce_boncard\BoncardInterface $boncard */
    $boncard = $this->entity;

    try {
      $trxId = 'REFUND' . rand(10000000, 99999999);
      $this->client->refund($boncard, $amount, $trxId);
      $this->client->submissionRequest($boncard, $amount, $trxId);
    }
    catch (BoncardException $e) {
      $this->messenger()->addError($e->getMessage());
      $form_state->setError($inline_form['amount'], $e->getMessage());
    }
  }

}
