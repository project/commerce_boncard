# Generate translations with potx module.

```
drush potx single --folder=modules/custom/commerce_boncard --api=9 --destination=modules/custom/commerce_boncard/translations --translations --language=de --outfile=translations-de
drush potx single --folder=modules/custom/commerce_boncard --api=9 --destination=modules/custom/commerce_boncard/translations --translations --language=fr --outfile=translations-fr
drush potx single --folder=modules/custom/commerce_boncard --api=9 --destination=modules/custom/commerce_boncard/translations --translations --language=it --outfile=translations-it
```
